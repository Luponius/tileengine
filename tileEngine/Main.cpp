//Copyright (c) <2013> <Bennet Vella> [Lonehwolf@hotmail.co.uk]
//
//This software is provided 'as-is', without any express or implied
//warranty. In no event will the authors be held liable for any damages
//arising from the use of this software.
//
//Permission is granted to anyone to use this software for any purpose,
//including commercial applications, and to alter it and redistribute it
//freely, subject to the following restrictions:
//
//   1. The origin of this software must not be misrepresented; you must not
//   claim that you wrote the original software. If you use this software
//   in a product, an acknowledgment in the product documentation would be
//   appreciated but is not required.
//
//   2. Altered source versions must be plainly marked as such, and must not be
//   misrepresented as being the original software.
//
//   3. This notice may not be removed or altered from any source
//   distribution.

#include <SFML/Graphics.hpp>

int main ()
{
	// ######### DECLARATIONS ######### //
	// ##### APP ##### //
	sf::RenderWindow app (sf::VideoMode(800,600,32),"Tile Engine");
	app.setVerticalSyncEnabled(true);

	// ##### TEX ##### //
	sf::Image imgFront;
	imgFront.loadFromFile("Assets/Foreground.png");
	imgFront.createMaskFromColor(sf::Color(163,73,164,255));
	sf::Image imgBkgrnd;
	sf::Texture texBkgrnd; // Stores any background images
	sf::Texture texFront; // Contains foreground objects
	texFront.loadFromImage(imgFront);
	//texBkgrnd.loadFromImage(imgFront);
	
	// ##### VECT POSITIONS ##### //
	sf::Vector2i iVecSizeSheet(96,128);
	sf::Vector2i iVecSizeMed(32,32);
	sf::Vector2i iVecSizeSmall(16,16);

	sf::Vector2i iVecPosPlayer(0,0);
	sf::Vector2i iVecPosEnemy(0,128);
	sf::Vector2i iVecPosBrick(0,256);
	sf::Vector2i iVecPosDirt(0,272);
	sf::Vector2i iVecPosSign(0,288);

	// ##### SPR##### //
	sf::Sprite sprLand(texBkgrnd); // Stores landscape
	sf::Sprite sprEnv(texBkgrnd); // Stores nearby environment
	sf::Sprite sprPlayer(texFront,sf::IntRect(iVecPosPlayer,iVecSizeMed)); // Stores player sprite
	sf::Sprite sprEnemy (texFront,sf::IntRect(iVecPosEnemy,iVecSizeMed)); // Stores enemy sprite
	sf::Sprite sprBrick (texFront,sf::IntRect(iVecPosBrick,iVecSizeSmall)); // Stores ground tile data
	sf::Sprite sprDirt (texFront,sf::IntRect(iVecPosDirt,iVecSizeSmall)); // Stores ground tile data
	sf::Sprite sprProp (texFront,sf::IntRect(iVecPosSign,iVecSizeMed)); // Stores prop tile data

	// ##### TIME ##### //
	sf::Clock clkGameTime;

	/* manually specified order multiplied by vertical spacing */
	enum sectFront { player, enemy, ground, prop };

	/*
	Calculating Frame Section and Animation
	To calculate the frame section required and animation level we need to calculate the x "frame" offset as well as the y "section" offset.
	*/

	// Condition for adding frame based on time:
	float fElapsed = 0;
	float fFrameCurrent = 0;
	float fFrameDuration = 0.25f;
	int iFrame = 0;
	int frameCount = 3;
	sf::Vector2i iVecAnimationFrame(0,0);
	sprPlayer.setPosition(100,400);
	// ##### GAME LOOP ##### //
	while (app.isOpen())
	{
		// ##### EVENT LOOP ##### //
		sf::Event event;
		while (app.pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				app.close();
				break;
			default:
				break;
			}
		}// ##### END EVENTS ##### //

		// Calculate Animation Frames
		fElapsed = clkGameTime.restart().asSeconds(); // get elapsed time
		fFrameCurrent += fElapsed;  // Keep track of total time
		if (fFrameCurrent >= fFrameDuration) // If time passed update
		{
		   iFrame++;
		   fFrameCurrent = 0;  // Reset frame
		}

		// Reset frame if exceeds total frames available
		if (iFrame > frameCount - 1)
		   iFrame = 0;

		iVecPosPlayer.x = 32*iFrame;

		sprPlayer.setTextureRect(sf::IntRect(iVecPosPlayer,iVecSizeMed));

		sf::IntRect rectPlayer = sf::IntRect(iFrame*32,player*32,32,32);
		sf::IntRect rectEnemy = sf::IntRect(iFrame*32,enemy*32,32,32);

		// ##### BACKGROUND RECTS ##### //
		sf::IntRect rectLand = sf::IntRect(0,0,600,480);
		sf::IntRect rectEnv = sf::IntRect(0,480,600,480);

		// ##### DRAWING ##### //
		sprProp.setPosition(50,400);
		// Clear to grey
		app.clear(sf::Color(0,128,225,255));

		//app.draw(sprLand);
		//app.draw(sprEnv);
		
		for (int i = 0; i < (app.getSize().x / 16); i++)
		{
			sprDirt.setPosition(i*16,432);
			app.draw(sprDirt);
			sprDirt.setPosition(i*16,448);
			app.draw(sprDirt);
		}
		
		app.draw(sprProp); //To possibly modify sprProp with more detail
		//app.draw(sprEnemy);
		app.draw(sprPlayer); // Draw player last for visibilty
		// Show drawn content
		app.display ();
	}
}